#!/usr/bin/env python3.6
"""A simple tool for printing out information on ELF files. Requires python 3.6 and pyelftools."""

import sys
import os.path
from typing import Tuple, List

from elftools.elf.elffile import ELFFile
from elftools.elf.sections import SymbolTableSection, Section

PRINT_SECTIONS = 1
PRINT_HEADERS = 2
PRINT_SYMBOLS = 4
PRINT_ALL = PRINT_SECTIONS | PRINT_HEADERS | PRINT_SYMBOLS


def parse_args(args: List[str]) -> Tuple[int, str]:
    """Parses the option flags from command line. Throws InvalidArgumentException if the format is invalid."""

    flags = 0
    flags_parsed = False
    filename = None
    for arg in args[1:]:
        print(arg)
        if arg == '--' or not arg.startswith('-'):
            flags_parsed = True
        if arg.startswith('-') and not flags_parsed:
            print('Here2')
            for flag in arg[1:]:
                if flag == 'h':
                    flags |= PRINT_HEADERS
                elif flag == 's':
                    flags |= PRINT_SYMBOLS
                elif flag == 'S':
                    flags |= PRINT_SECTIONS
                else:
                    raise InvalidArgumentException()
        elif flags_parsed and filename:
            raise InvalidArgumentException()
        else:
            filename = arg
    return (flags, filename) if flags else (PRINT_ALL, filename)


def get_file_info(fd: ELFFile, opts: int) -> None:
    """Prints out the necessary info."""
    if opts & PRINT_HEADERS:
        endianness = 'little-endian' if fd.little_endian else 'big-endian'
        print(f'Endianness: {endianness}', 
              f'Architecture: {fd.get_machine_arch()}'
        )
        print('HEADER')
        for item in fd.header:
            print(f'{item}: {fd.header[item]}')
    if opts & PRINT_SECTIONS:
        for sec in fd.iter_sections():
            print('============')
            print(f'Section: {sec.name}\tType: {sec["sh_type"]}\t'
                    f'Size: {sec["sh_size"]}')
            if opts & PRINT_HEADERS:
                print('============')
                for item in sec.header:
                    print(f'{item}: {sec.header[item]}')

    if opts & PRINT_SYMBOLS:
        sec = fd.get_section_by_name(',symtab')

        if not sec:
            print('Symbol table empty. Probably the file was stripped')
        else:
            num_symbols = sec.num_symbols()
            print(f'There are {num_symbols} symbols')
            for i in range(num_symbols):
                sym = sec.get_symbol[i]
                print('============')
                print(f'Symbol name: {sym["st_name"]}')
                print(f'Value: {sym["st_value"]}')
                print(f'Size: {sym["st_size"]}')
                print(f'Info: {sym["st_info"]}')


def main() -> None:
    """Main entry point of the application."""
    flags, filename = parse_args(sys.argv)
    print(f'Analysing file {os.path.basename(filename)}\n')
    try:
        with open(filename, 'rb') as f:
            get_file_info(ELFFile(f), flags)
    except FileNotFoundError:
        print('No such file!')
    except:
        print('An error occurred! Aborting...')


if __name__ == '__main__':
    main()

