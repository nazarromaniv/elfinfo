from setuptools import setup, find_packages

setup(
        name='elfinfo',
        version='0.1',
        scripts=['elfinfo.py'],
        python_requires='>=3.6',
        
        packages=find_packages(),
        entry_points={
            'console_scripts': ['elfinfo = src.elfinfo:main']
        },

        install_requires=['pyelftools'],
        author='Nazar Romaniv',
        author_email='romaniv_ak18@nuwm.edu.ua',
        license='MIT'
)

