## Elfinfo

A simple tool to print out information on ELF binaries

Usage: `elfinfo [-h|s|S] [--] filename`  
 -h  print only header information (if -S is specified, it will print each section's header)  
 -s  print symbol table information  
 -S print each sections information  

The default is all flags on. -- can be used to specify the end of flags.
It can be used if the name of the file starts with -.
